clc;clear all;close all;
%% Prelab 9
%% form matrix and set parameters
EkN_dB = 10;
EkN = 10^(EkN_dB/10);
C = 0:0.01:1;                        %plot the parameters according to different C
for k = 1:length(C)
    for i = 1:1000                  % 1000 realizations
        Var_H = 1-C(k);
        % form H matrix
        H = 0.5*C(k)*(ones(2,2)) +0.5*(sqrt(Var_H)*randn(2,2)+1j*sqrt(Var_H)*randn(2,2));
        %H = 0.5*C(k)*([1 0;0 1]) +0.5*(sqrt(Var_H)*randn(2,2)+1j*sqrt(Var_H)*randn(2,2));
%% eigenvalue decomposition
        [V,S] = eig(H*H');% find the eigenvalue of H*H'
        H_gain = diag(S);
        % calculate effective E/N
        EN_1(i) = H_gain(1)*EkN;
        EN_2(i) = H_gain(2)*EkN;
        % calculate effective E/N in dB scale
        EN_1_dB(i) = 10*log10(H_gain(1)*EkN);
        EN_2_dB(i) = 10*log10(H_gain(2)*EkN);
        % calculate subchannel capacity
        C_1(i) = log2(1+H_gain(1)*EkN);
        C_2(i) = log2(1+H_gain(2)*EkN);
    end
%% plot the histogram
    if (k==1)|(k==51)|(k==100)|(k==101)
        figure
        subplot(2,2,1)
        hist(EN_1,20);
        title('Histogram of effective E1/N');
        subplot(2,2,2)
        hist(EN_2,20);
        title('Histogram of effective E2/N');
%         figure
        subplot(2,2,3)
        hist(C_1,20);
        title('Histogram of C1');
        subplot(2,2,4)
        hist(C_2,20);
        title('Histogram of C2');
    end
%% average among 1000 realizations
    EN_1_ave(k) = mean(EN_1);                           %Effective E/N for channel 1
    EN_2_ave(k) = mean(EN_2);                           %Effective E/N for channel 2
    EN_1_var(k) = var(EN_1);
    EN_2_var(k) = var(EN_2);
    EN_1_dB_ave(k) = mean(EN_1_dB);                     %Effective E/N in dB for channel 1
    EN_2_dB_ave(k) = mean(EN_2_dB);                           %Effective E/N in dB for channel 2
    C_1_ave(k) = mean(C_1);                                   %capacity for channel 1
    C_2_ave(k) = mean(C_2);                                   %capacity for channel 2   
    P_outage_1(k) = sum(EN_1_dB<7)/length(EN_1_dB);           %outage prob for channel 1
    P_outage_2(k) = sum(EN_2_dB<7)/length(EN_2_dB);           %outage prob for channel 2
    P_outage_total(k) = (sum(EN_1_dB<7)+sum(EN_2_dB<7))/(length(EN_1_dB)+length(EN_2_dB));
end
%% plot the effective S/N(in dB and not in dB), capacity, outage prob for channel 1 and 2 for different C 
figure;
subplot(141);
plot(C,EN_1_ave,'r');hold on;
plot(C,EN_2_ave,'g');grid on;
xlabel('C');ylabel('Effective S/N');
legend('Channel 1','channel 2');
title('Effective S/N');
subplot(142);
plot(C,10*log10(EN_1_ave),'r');hold on;
plot(C,10*log10(EN_2_ave),'g');grid on;
xlabel('C');ylabel('Effective S/N in dB');
legend('Channel 1','channel 2');
title('Effective S/N in dB');
subplot(143);
plot(C,C_1_ave,'r');hold on;
plot(C,C_2_ave,'g');grid on;
xlabel('C');ylabel('subchannel capacity');
legend('Channel 1','channel 2');
title('Subchannel capacity');
subplot(144);
plot(C,P_outage_1,'r');hold on;
plot(C,P_outage_2,'g');grid on;
xlabel('C');ylabel('Outage Probability');
legend('Channel 1','channel 2');
title('Subchannel outage probability');
%% average over two subchannels;
figure;
plot(C,P_outage_total);title('average outage probability');
xlabel('C');ylabel('Outage Probability');
%% compare SISO with MIMO with total capacity
C_SISO = log2(1+EkN)*ones(size(C));
C_MIMO = C_1_ave+C_2_ave;
figure;
plot(C,C_SISO,'r');hold on;
plot(C,C_MIMO);hold off;grid on;
legend('SISO','MIMO');







